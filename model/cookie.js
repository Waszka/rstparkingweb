function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (60 * 60 * 24 * 1000));
    if (document.domain === 'localhost') {
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + ";path=/";
    } else {
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + ';domain=.' + document.domain + ";path=/";
    }

}

function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

function deleteCookie(key) {
    if (document.domain === 'localhost') {
        document.cookie = key + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';
    } else {
        document.cookie = key + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;domain=.' + document.domain + ';path=/';
    }

}