const apiUrl = "http://bartolini.rst.com.pl/api";

function postPlace(id, parkingId, json, width, height) {
    $.ajax({
        type: 'POST',
        url: apiUrl + '/v1/parking/' + parkingId + '/place',
        headers: {
            "X-APP-TOKEN": getCookie('token')
        },
        crossDomain: true,
        data: JSON.stringify(json),
        dataType: 'json',
        contentType: "application/json",
        success: function (data) {
            $.get('view/template/template.html', function (templates) {
                let rgb = getRandomRGB();
                let templateDataList = {
                    rgb: rgb,
                    id: id,
                    placeId: data.id,
                    name: data.name
                };

                let templateList = $(templates).filter('#tmp-placeList').html();
                $('#placeList' + id).append(Mustache.render(templateList, templateDataList));
                drawFigure(json, 'canvasBackground' + id, rgb);
                clearCanvas(id, width, height);

                alertPopup('alert-success', '+', 'Pomyślnie dodano miejsce parkingowe', id);


            });


        },
        statusCode: {
            204: function () {
                console.log('No content');
                alertPopup('alert-danger', 204, 'Wystąpił błąd przepraszamy', id);
            },
            400: function () {
                console.log('Bad Request');
                alertPopup('alert-danger', 400, 'Wystąpił błąd przepraszamy', id);
            },
            401: function () {
                console.log('Unauthorized');
                alertPopup('alert-danger', 401, 'Błąd autoryzacji', id);
            },
            403: function () {
                console.log('Forbidden');
                alertPopup('alert-danger', 403, 'Błąd autoryzacji', id);
            },
            404: function () {
                console.log('Not Found');
                alertPopup('alert-danger', 404, 'Błąd', id);
            },
            415: function () {
                console.log('Unsupported Media Type');
                alertPopup('alert-danger', 415, 'Błąd', id);
            }
        },

        error: function (e) {
            console.log("Server error - " + e.status);
        }

    });

}

function postParking(json, cameraId, width, height) {
    $.ajax({
        type: 'POST',
        url: apiUrl + '/v1/parking',
        headers: {
            "X-APP-TOKEN": getCookie('token')
        },
        crossDomain: true,
        data: JSON.stringify(json),
        dataType: 'json',
        contentType: "application/json",
        success: function (data) {
            let numberOfAccordionElement = $('#accordion').contents().length;
            $.get('view/template/template.html', function (templates) {

                let id = Number(getCookie('numberOfParkings')) + 1;
                setCookie('numberOfParkings', id);

                let templateDataSection = {
                    name: data.name,
                    id: id,
                    parkingId: data.id,
                    width: width,
                    height: height
                };

                let templateSection = $(templates).filter('#tmp-section').html();
                $('#accordion').append(Mustache.render(templateSection, templateDataSection));
                if (numberOfAccordionElement === 1 || Number(getCookie('numberOfParkings')) === 1) {
                    $('#createParkingBtn').remove();
                    $("#accordion").accordion({
                        collapsible: true
                    });
                }
                $('#accordion').accordion('refresh');

                getCameraImage(id, cameraId, width, height, data.places, templates, data.id);

                $("#submitId" + id).attr("disabled", true);
                countCanvasClick(id, data.id, width, height);


            });

            addCamera(cameraId, data.id);

            $.modal.close();
            $('#parkingName').val('');
            alertPopup('alert-success', '+', 'Pomyślnie dodano miejsce parkingowe');


        },
        statusCode: {
            204: function () {
                console.log('No content');
                alertPopup('alert-danger', 204, 'Wystąpił błąd przepraszamy');
            },
            400: function () {
                console.log('Bad Request');
                alertPopup('alert-danger', 400, 'Wystąpił błąd przepraszamy');
            },
            401: function () {
                console.log('Unauthorized');
                alertPopup('alert-danger', 401, 'Błąd autoryzacji');
            },
            403: function () {
                console.log('Forbidden');
                alertPopup('alert-danger', 403, 'Błąd autoryzacji');
            },
            404: function () {
                console.log('Not Found');
                alertPopup('alert-danger', 404, 'Błąd');
            },
            415: function () {
                console.log('Unsupported Media Type');
                alertPopup('alert-danger', 415, 'Błąd');
            }
        },

        error: function (e) {
            console.log("Server error - " + e.status);
        }

    });

}

function getParking(width, height) {
    $.ajax({
        type: 'GET',
        url: apiUrl + '/v1/parking?width=' + width + '&height=' + height,
        headers: {
            "X-APP-TOKEN": getCookie('token')
        },
        crossDomain: true,
        dataType: 'json',
        beforeSend: function () {
            $("#loadingGif").show();
        },
        statusCode: {
            503: function () {
                $('#loadingGif').append("<p><strong>Nasze serwery są obecnie nie dostępne przepraszamy</strong></p>");
                setTimeout(function () {
                    deleteCookie('session');
                    deleteCookie('numberOfParkings');
                    deleteCookie('token');
                    window.location.reload();
                }, 5000);
            }
        },
        success: function (data, textStatus, xhr) {
            $("#loadingGif").hide();
            if (xhr.status === 204) {
                setCookie('numberOfParkings', 0);
                $('#accordion').append("<button id='createParkingBtn' type='button' class='btn btn-outline-primary' onclick='parkingModal(" + width + "," + height + ")' >Dodaj parking</button>");
            } else {
                let jsonString = JSON.stringify(data);
                let response = JSON.parse(jsonString);

                generateSection(response, width, height);
            }
        },
        error: function (xhr) {
            if (xhr.status === 0) {
                $('#loadingGif').append("<p><strong>Nasze serwery są obecnie nie dostępne przepraszamy</strong></p>");
                setTimeout(function () {
                    deleteCookie('session');
                    deleteCookie('numberOfParkings');
                    deleteCookie('token');
                    window.location.reload();
                }, 5000);
            }
            console.log(xhr.status);
        }
    });
}

function getListOfCamera() {
    $.ajax({
        type: 'GET',
        url: apiUrl + '/v1/camera',
        headers: {
            "X-APP-TOKEN": getCookie('token')
        },
        crossDomain: true,
        dataType: 'json',
        success: function (data, textStatus, xhr) {

            $('#cameraSelect').empty().append('<option selected disabled hidden>Wybierz kamerę...</option>');
            for (let i = 0; i < data.length; i++) {
                $('#cameraSelect').append('<option value="' + data[i].id + '">' + data[i].name + '</option>')
            }
        },
        error: function (xhr) {
            console.log(xhr.status);
        }
    });
}

function addCamera(cameraId, parkingId) {
    $.ajax({
        type: 'PUT',
        url: apiUrl + '/v1/parking/' + parkingId + '/camera',
        headers: {
            "X-APP-TOKEN": getCookie('token')
        },
        data: cameraId,
        crossDomain: true,
        contentType: "application/json",
        success: function () {

        },
        error: function (xhr) {
            console.log(xhr.status);
        }
    });
}


function getCameraImage(id, cameraId, width, height, placeList, templates, parkingId) {
    $.ajax({
        type: 'GET',
        url: apiUrl + '/v1/camera/' + cameraId + '/image?width=' + width + '&height=' + height,
        crossDomain: true,
        beforeSend: function () {
            $("#loadingImage" + id).show();
            $('#canvas' + id).addClass('loading-click');
        },
        xhr: function () {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob'
            return xhr;
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 204) {
                $('#loadingImage' + id).append("<p><strong>Brak obrazu z kamery spróbuj później</strong></p>");
                $('#canvas' + id).remove();
                $('#canvasBackground' + id).remove();
            } else {
                let img = new Image();
                img.onload = function () {
                    $("#loadingImage" + id).hide();
                    $('#canvas' + id).removeClass('loading-click').addClass('active-click');
                    imageBackgroundSize(id, img);
                    generateListOfPlaces(id, placeList, templates);
                    countCanvasClick(id, parkingId, width, height);
                };
                var url = window.URL || window.webkitURL;
                img.src = url.createObjectURL(data);
            }
        },
        error: function (xhr) {
            console.log(xhr.status);
        }
    });
}


function postLogin() {

    if (!($('#login').val() === '' || $('#password').val() === '')) {

        let json = {
            'login': $('#login').val(),
            'password': $('#password').val()
        };

        $.ajax({
            type: 'POST',
            url: apiUrl + '/v1/login',
            crossDomain: true,
            data: JSON.stringify(json),
            dataType: 'json',
            contentType: "application/json",
            success: function (data) {
                setCookie('token', data.token);
                $.ajax({
                    type: 'POST',
                    url: '../controller/addSession.php',
                    success: function () {
                        window.location.href = '../';
                    }
                });

            },
            statusCode: {
                400: function () {
                    console.log('Bad Request');
                    alertPopup('alert-danger', 400, 'Wystąpił błąd przepraszamy', false, "#loginAlert");
                },
                401: function () {
                    console.log('Unauthorized');
                    alertPopup('alert-danger', '!', 'Zły login lub hasło', false, "#loginAlert");
                },
                403: function () {
                    console.log('Forbidden');
                    alertPopup('alert-danger', 403, 'Błąd autoryzacji', false, "#loginAlert");
                },
                404: function () {
                    console.log('Not Found');
                    alertPopup('alert-danger', 404, 'Błąd', false, "#loginAlert");
                },
                415: function () {
                    console.log('Unsupported Media Type');
                    alertPopup('alert-danger', 415, 'Błąd', false, "#loginAlert");
                },
                503: function () {
                    alertPopup('alert-danger', '!', 'Serwery niedostępne, przepraszamy', false, "#loginAlert");
                }
            },

            error: function (e) {
                if (e.status === 0) {
                    alertPopup('alert-danger', '!', 'Serwery niedostępne, przepraszamy', false, "#loginAlert");
                }
                $('#password').val('');
                console.log("Server error - " + e.status);

            }

        });
    }
}

function logout() {
    $.ajax({
        type: 'POST',
        url: 'controller/deleteSession.php',
        success: function () {
            deleteCookie('numberOfParkings');
            deleteCookie('token');
            window.location.reload()
        }
    });

}

function deleteParking(id, parkingId, width, height) {
    if (confirm('Czy na pewno skasować parking?')) {

        $.ajax({
            url: apiUrl + '/v1/parking/' + parkingId,
            headers: {
                "X-APP-TOKEN": getCookie('token')
            },
            type: 'DELETE',
            crossDomain: true,
            statusCode: {
                204: function () {
                    console.log('No content');
                    alertPopup('alert-danger', 204, 'Wystąpił błąd przepraszamy');
                },
                400: function () {
                    console.log('Bad Request');
                    alertPopup('alert-danger', 400, 'Wystąpił błąd przepraszamy');
                },
                401: function () {
                    console.log('Unauthorized');
                    alertPopup('alert-danger', 401, 'Błąd autoryzacji');
                },
                403: function () {
                    console.log('Forbidden');
                    alertPopup('alert-danger', 403, 'Błąd autoryzacji');
                },
                404: function () {
                    console.log('Not Found');
                    alertPopup('alert-danger', 404, 'Błąd nie znaleziono parkingu, odśwież strone');
                }
            },
            success: function () {
                alertPopup('alert-info', '+', 'Parking został usunięty');
                $('#accordionHeader' + id).remove();
                $('#accordionBody' + id).remove();

                setCookie('numberOfParkings', Number(getCookie('numberOfParkings')) - 1);
                if (Number(getCookie('numberOfParkings')) === 0) {
                    $('#accordion').append("<button id='createParkingBtn' type='button' class='btn btn-outline-primary' onclick='parkingModal(" + width + "," + height + ")' >Dodaj parking</button>");
                } else {
                    $('#accordion').accordion('refresh');
                }


            },
            error: function (xhr) {
                console.log(xhr.status);
            }
        });
    }
}

function deletePlace(id, placeId) {
    if (confirm('Czy na pewno skasować miejsce parkingowe?')) {
        $.ajax({
            url: apiUrl + '/v1/place/' + placeId,
            headers: {
                "X-APP-TOKEN": getCookie('token')
            },
            type: 'DELETE',
            crossDomain: true,
            statusCode: {
                204: function () {
                    console.log('No content');
                    alertPopup('alert-danger', 204, 'Wystąpił błąd przepraszamy', id);
                },
                400: function () {
                    console.log('Bad Request');
                    alertPopup('alert-danger', 400, 'Wystąpił błąd przepraszamy', id);
                },
                401: function () {
                    console.log('Unauthorized');
                    alertPopup('alert-danger', 401, 'Błąd autoryzacji', id);
                },
                403: function () {
                    console.log('Forbidden');
                    alertPopup('alert-danger', 403, 'Błąd autoryzacji', id);
                },
                404: function () {
                    console.log('Not Found');
                    alertPopup('alert-danger', 404, 'Błąd nie znaleziono miejsca parkingowego, odśwież strone', id);
                }
            },
            success: function () {
                setCookie('isDeleted', true);
                location.reload(true);
            },
            error: function (xhr) {
                console.log(xhr.status);
            }
        });
    }
}


