function init() {

    let isDeleted = getCookie('isDeleted');
    if(isDeleted === 'true'){
        alertPopup('alert-info', '+', 'Miejsce parkingowe zostało usunięte');
        deleteCookie('isDeleted');
    }

    let width = 1024;
    let height = 600;
    getParking(width, height);
    $('#openModal').on('click', function () {
        parkingModal(width, height);
    });
}

function generateSection(response, width, height) {
    $.get('view/template/template.html', function (templates) {

        for (let i = 0; i < response.length; i++) {

            let parking = response[i];
            let templateDataSection = {
                name: parking.name,
                id: i,
                parkingId: parking.id,
                width: width,
                height: height
            };

            let templateSection = $(templates).filter('#tmp-section').html();
            $('#accordion').append(Mustache.render(templateSection, templateDataSection));
            $("#submitId" + i).attr("disabled", true);

            getCameraImage(i, parking.camera.id, width, height, response[i].places, templates, parking.id);

        }

        setCookie('numberOfParkings', response.length);
        $("#accordion").accordion({
            collapsible: true
        });

    });

}

function generateListOfPlaces(id, places, templates) {

    for (let j = 0; j < places.length; j++) {
        let rgb = getRandomRGB();
        let placeJson = places[j].quadrangle;
        let templateDataList = {
            rgb: rgb,
            id: id,
            placeId: places[j].id,
            name: places[j].name
        };
        let templateList = $(templates).filter('#tmp-placeList').html();
        $('#placeList' + id).append(Mustache.render(templateList, templateDataList));

        drawFigure(placeJson, 'canvasBackground' + id, rgb);
    }

}

function parkingModal(width, height) {

    $("#parkingModal").modal({
        showClose: false,
        fadeDuration: 400,
        fadeDelay: 0.50
    });


    $('#closeModalHeader').on('click', function () {
        $.modal.close();
    });
    $('#closeModalFooter').on('click', function () {
        $.modal.close();
    });

    $('#submitParking').attr("disabled", true);
    checkModal('#parkingName','#cameraSelect' , '#submitParking');

    getListOfCamera();

    $('#submitParking').off().on('click', function (e) {
        e.preventDefault();
        let cameraId = $('#cameraSelect').val();
        let json = {
            name: $('#parkingName').val()
        };
        postParking(json, cameraId, width, height);
    });

}

function checkModal(checkedInput, checkedSelect, submit, id = '') {

    if ($(checkedInput + id).val() !== '' && $(checkedSelect + id).val() !== null) {
        $(submit + id).attr("disabled", false)
    } else {
        setTimeout(function () {
            checkModal(checkedInput, checkedSelect, submit, id);
        }, 100);
    }
}


function alertPopup(alertType, strongText, msg, id = false, alertId = '#topAlert') {
    let path;
    if(alertId === '#topAlert'){
        path = 'view/template/template.html';
    }else{
        path = 'template/template.html'
    }
    $.get(path, function (templates) {

        let alertTemplate = $(templates).filter('#tmp-alert').html();
        if (id === false) {
            $(alertId).append(Mustache.render(alertTemplate, {
                alertType: alertType,
                strongText: strongText,
                msg: msg
            }));
        } else {
            $('#alert' + id).append(Mustache.render(alertTemplate, {
                alertType: alertType,
                strongText: strongText,
                msg: msg
            }));
        }
        $('#alertTmp').hide().slideDown('slow').delay(1000).slideUp("slow", function () {
            $('#alertTmp').remove();
        });
    });
}