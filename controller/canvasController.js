function getPosition(event, id) {
    let canvas = document.getElementById("canvas" + id);
    let rect = canvas.getBoundingClientRect();
    let x = Math.floor(( event.clientX - rect.left ) / ( rect.right - rect.left ) * canvas.width);
    let y = Math.floor(( event.clientY - rect.top ) / ( rect.bottom - rect.top ) * canvas.height);
    drawPoint(x, y, id);
    return {x, y};
}

function drawPoint(x, y, id) {
    let ctx = document.getElementById("canvas" + id).getContext("2d");

    ctx.fillStyle = "#ff3414";

    ctx.beginPath();
    ctx.arc(x, y, 2, 0, Math.PI * 2, true);
    ctx.fill();
}

function countCanvasClick(id, parkingId, width, height) {
    let count = 0;
    let coordinateJson = {};
    $("#canvas" + id).off().click(function (e) {
        count += 1;
        let newX = 'x' + count;
        let newY = 'y' + count;
        coordinateJson[newX] = getPosition(e, id).x;
        coordinateJson[newY] = getPosition(e, id).y;
        if (count === 4) {

            drawFigure(coordinateJson, 'canvas' + id, "#ff3414");
            $('#canvas' + id).unbind('click');

            checkValue('#placeId', '#submitId', id);
            $('#submitId' + id).off().on('click', function (e) {
                e.preventDefault();
                coordinateJson['name'] = $('#placeId' + id).val();
                coordinateJson['imageWidth'] = width;
                coordinateJson['imageHeight'] = height;
                postPlace(id, parkingId, coordinateJson, width, height)
            });
        }
    });

}

function checkValue(checkedInput, submit, id = '') {

    if ($(checkedInput + id).val() !== '') {
        $(submit + id).attr("disabled", false)
    } else {
        setTimeout(function () {
            checkValue(checkedInput, submit, id)
        }, 100);
    }
}

function drawFigure(coordinateJson, layer, rgb) {

    let canvas = document.getElementById(layer);
    let ctx = canvas.getContext("2d");

    ctx.beginPath();
    ctx.moveTo(coordinateJson.x1, coordinateJson.y1);
    ctx.lineTo(coordinateJson.x2, coordinateJson.y2);
    ctx.lineTo(coordinateJson.x3, coordinateJson.y3);
    ctx.lineTo(coordinateJson.x4, coordinateJson.y4);
    ctx.lineTo(coordinateJson.x1, coordinateJson.y1);

    ctx.lineTo(coordinateJson.x1, coordinateJson.y1);
    ctx.lineTo(coordinateJson.x3, coordinateJson.y3);

    ctx.lineTo(coordinateJson.x2, coordinateJson.y2);
    ctx.lineTo(coordinateJson.x4, coordinateJson.y4);

    ctx.lineWidth = 2;
    ctx.strokeStyle = rgb;
    ctx.stroke();

}

function imageBackgroundSize(i, img) {
    let canvas = document.getElementById('canvas' + i);
    let canvas1 = document.getElementById('canvasBackground' + i);
    let ctx1 = canvas1.getContext('2d');

    canvas.width = img.width;
    canvas.height = img.height;

    canvas1.width = img.width;
    canvas1.height = img.height;
    ctx1.drawImage(img, 0, 0);

}


function clearCanvas(id, width, height) {

    let canvas = document.getElementById('canvas' + id);
    let ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    $('#placeId' + id).val('');
    $("#submitId" + id).attr("disabled", true);
    $('#canvas' + id).bind('click');
    countCanvasClick(id, $('#parkingId' + id).val(), width, height);


}

function getRandomRGB() {
    let min = Math.ceil(0);
    let max = Math.floor(255);
    let first = Math.floor(Math.random() * (max - min)) + min;
    let second = Math.floor(Math.random() * (max - min)) + min;
    let third = Math.floor(Math.random() * (max - min)) + min;
    return 'rgb(' + first + ', ' + second + ', ' + third + ')';
}
