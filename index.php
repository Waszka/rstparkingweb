<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico"/>

    <script src="js/jquery-3.4.1.js"></script>
    <script src="js/jcanvas.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.0.1/mustache.js"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <script src="assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="js/script.js"></script>
    <script src="model/cookie.js"></script>
    <script src="model/apiConnection.js"></script>
    <script src="controller/canvasController.js"></script>
    <script src="controller/parkingController.js"></script>

    <?php
    session_start();
    if (!isset($_SESSION['session']) || $_SESSION['session'] != true) {
        header("Location:view/login.html");
    }
    ?>

    <script>
        $(document).ready(function () {
            init();
        });
    </script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css"/>

    <meta charset="UTF-8">
    <title>Bartolini</title>
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #848992;">
    <a class="navbar-brand" href="#">Bartolini</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Strona Główna <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a id="openModal" class="nav-link" style="cursor: pointer">Dodaj Parking <span class="sr-only"></span></a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" style="cursor: pointer"
                   onclick="logout()">Wyloguj</a>
            </li>
        </ul>
    </div>
</nav>


<div id="topAlert"></div>


<!-- The Modal -->
<div class="modal" id="parkingModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Dodaj Parking</h4>
                <button id="closeModalHeader" type="button" class="close">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="input-group mb-3">
                    <input id="parkingName" type="text" class="form-control" placeholder="Nazwa parkingu">
                </div>
                <div class="input-group">
                    <select class="form-control" id="cameraSelect">

                    </select>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button id="submitParking" type="button" class="btn btn-outline-primary">Dodaj</button>
                <button id="closeModalFooter" type="button" class="btn btn-outline-danger">Close</button>
            </div>

        </div>
    </div>
</div>


<div id="loadingGif" class="text-center align-item-center m-5">
    <img src="assets/img/loading.gif">
</div>


<div id="accordion"></div>

</body>

<footer class="page-footer font-small">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">©
        <script>
            document.write(new Date().getFullYear())
        </script>
        Copyright:
        <a href="https://rst.software/"> RST Software Masters</a>
    </div>
    <!-- Copyright -->

</footer>

</html>